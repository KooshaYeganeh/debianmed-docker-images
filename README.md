# DebianMed-Docker-Images


A repository containing Debian Medical Dockerfiles by specialization and branch

In this repository, we have tried and will try to make the Debian Medical system more known and fit more in the treatment system.


### Branches


**1 - Imaging**  
**2 - Laboratory**  
**3 - Dental**






### Steps to Run GUI Application Inside Docker Container

#### Step 1 – Start Docker Services

To make use of the docker service and check its status, use the below commands to check the status:

```
systemctl start docker
```
```
systemctl status docker
```

#### Step 2 – Pull the Centos Image from DockerHub

This will by default pull the debian image from DockerHub with the latest version of debian and will store it in a local system.  So, that you can create the containers from the image. Use the below command to pull any image from DockerHub.

```
docker pull debian
```

#### Step 3 – Customized Docker Image Building

If we want to run a GUI program inside Docker Container let’s say for example ‘’firefox’’. So, what do we have to do? So, first, we have to install the software.  We can create a customized container image that has the required software to run the application. This process is known as creating a customized docker image. For this, the best approach to create a customized image is by creating a “Dockerfile”.

If you are creating a Dockerfile then the filename where you will be writing the script to create the image must be named “Dockerfile”

```
FROM debian:latest
RUN yum install firefox -y
CMD firefox
```


#### Step 4 – Now let’s build the image

```
docker build -t myfirefox:v1 .
```

Step4Once the build is done, let’s launch the container

```
docker run -it --name firefoxos1 myfirefox:v1
```

Here you will see errors, we are not able to launch the firefox container. Why?

The main logic here is firefox need one environmental variable to run itself. This variable helps firefox to project its GUI tab on the screen. Now we know that the docker container does not have a GUI screen to run the firefox program. So, we need to tell the container to use the host system GUI screen and for that, we are going to use that environmental variable by adding the env option in the command. So run the below command.

```
docker run -it --name firefoxos1 -–env=”DISPLAY” –net=”host” myfirefox:v1
```

Now there is one more option we are using in the above command i.e., net=”host”. Because we are goanna connect our docker container with your RHEL8 host system network card. This option is needed because after launching the container it will give you the GUI window, whatever you willsearch in the GUI window needs to go to the docker container via the docker host. For that connectivity, we used that option in a command.


Refrences :
https://www.cloudthat.com/resources/blog/run-gui-application-inside-docker-container
